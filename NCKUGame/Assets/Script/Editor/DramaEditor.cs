﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

public class DramaEditor : EditorWindow {

    public string[] DramasName = new string[0];
    public List<BaseDramaEvent> Drama = new List<BaseDramaEvent>();
    public int EditingDramaNameIndex = -1;
    public string EditingDramaName = null;
    [MenuItem("Editor/DramaEditor")]
    static void OpenWindow() {
        DramaEditor window = EditorWindow.GetWindow<DramaEditor>();
        window.LoadDramaName();
    }


    private void LoadDramaName() {
        DramasName = DramaFileManager.GetDramasName();
    }

    Vector2 dramasNameSrcollPos;
    private void OnGUI() {
        EditorGUILayout.BeginHorizontal();
        {
            DrawDramasName();
            DrawDrama();
        }
        EditorGUILayout.EndHorizontal();
    }

    private void DrawDramasName() {
        EditorGUILayout.BeginVertical("box",GUILayout.Width(200));
        {
            dramasNameSrcollPos = EditorGUILayout.BeginScrollView(dramasNameSrcollPos);
            {
                for (int i = 0; i < DramasName.Length; ++i) {
                    EditorGUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button("edit")) {
                            Drama = new List<BaseDramaEvent>(DramaFileManager.LoadDrama(DramasName[i]));
                            EditingDramaNameIndex = i;
                            EditingDramaName = DramasName[i];
                        }
                        EditorGUILayout.LabelField(DramasName[i]);
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndScrollView();
            if(GUILayout.Button("Add New Drama")) {
                DramaFileManager.SaveDrama("NewDrama", new BaseDramaEvent[0]);
                LoadDramaName();
            }

        }
        EditorGUILayout.EndVertical();
    }

    Vector2 dramaScrollPos;
    private void DrawDrama() {
        if (Drama == null) {
            return;
        }
        EditorGUILayout.BeginVertical("box");
        {
            if (Drama != null) {
                if (EditingDramaNameIndex >= 0) {
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField("DramaName", GUILayout.Width(150));
                        DramasName[EditingDramaNameIndex] = EditorGUILayout.TextField(DramasName[EditingDramaNameIndex], GUILayout.Width(200));
                    }
                    EditorGUILayout.EndHorizontal();

                    dramaScrollPos = EditorGUILayout.BeginScrollView(dramaScrollPos);
                    {
                        BaseDramaEvent toBeRemove = null;
                        for (int i = 0; i < Drama.Count; i++) {
                            EditorGUILayout.BeginHorizontal();
                            {
                                if (GUILayout.Button("x", GUILayout.Width(20))) {
                                    toBeRemove = Drama[i];
                                }
                                BaseDramaEvent.DramaEventType NewType;
                                NewType = (BaseDramaEvent.DramaEventType)EditorGUILayout.EnumPopup("", Drama[i].EventType, GUILayout.Width(150));
                                if (NewType != Drama[i].EventType) {
                                    Drama[i] = BaseDramaEvent.CreateEvent(NewType);
                                }
                                DrawEvent(Drama[i]);
                            }
                            EditorGUILayout.EndHorizontal();
                        }

                        if (toBeRemove != null) {
                            Drama.Remove(toBeRemove);
                        }

                    }
                    EditorGUILayout.EndScrollView();
                    if (GUILayout.Button("AddEvent")) {
                        Drama.Add(new WaitForPlayerInputEvent());
                    }
                    if (GUILayout.Button("Save")) {
                        //Todo , 加上 overwrite 警告。
                        //存一個新的
                        DramaFileManager.SaveDrama(DramasName[EditingDramaNameIndex], Drama.ToArray());
                        //檢查名字是否有改動
                        if (DramasName[EditingDramaNameIndex] != EditingDramaName) {
                            DramaFileManager.DeleteDrama(EditingDramaName);
                        }
                        LoadDramaName();
                    }
                }
            }
        }
        EditorGUILayout.EndVertical();

    }

    private void DrawEvent(BaseDramaEvent e) {
      

        e.DrawEditor();
    }

}
