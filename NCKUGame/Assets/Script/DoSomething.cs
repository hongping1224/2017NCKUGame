﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoSomething : MonoBehaviour {

    public void CreateImageObject() {
        GameObject prefab = Resources.Load<GameObject>("Prefab/Image");
        GameObject go =  Instantiate(prefab);
        go.transform.parent = transform.parent;
        go.name = "abc";
    }
    public void ChangeImageObjectSprite() {
        Sprite sprite = Resources.Load<Sprite>("Sprite/pause");
        /*  GameObject go = GameObject.Find("abc");
          Image img = go.GetComponent<Image>();
          img.sprite = sprite;*/
        GameObject.Find("abc").GetComponent<Image>().sprite = sprite;
    }
    public void ChangeImageObjectPosition() {
        GameObject go = GameObject.Find("abc");
        go.transform.localPosition = new Vector3(0, 0, 0);
     /*   go.transform.eulerAngles = new Vector3(0, 0, 0);
        go.transform.localScale = new Vector3(0, 0, 0);*/
    }
}
