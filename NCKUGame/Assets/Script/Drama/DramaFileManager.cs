﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class DramaFileManager {

    public const string SavePath = "Resources/Drama/";
    public const string fileExtension = ".xml";

    public static void SaveDrama(string filename, BaseDramaEvent[] events) {
        string fileFullPath = Path.Combine(Application.dataPath, SavePath);
        if (Directory.Exists(fileFullPath) == false) {
            Directory.CreateDirectory(fileFullPath);
        }
        fileFullPath = Path.Combine(fileFullPath, filename + fileExtension);
        XMLManager.Save<BaseDramaEvent[]>(events, fileFullPath);
    }
    public static BaseDramaEvent[] LoadDrama(string filename) {
        string fileFullPath = Path.Combine(Path.Combine(Application.dataPath, SavePath), filename + fileExtension);
        if (File.Exists(fileFullPath)) {
            return XMLManager.Load<BaseDramaEvent[]>(fileFullPath);
        }
        return null;
    }


    public static void DeleteDrama(string filename) {
        string fileFullPath = Path.Combine(Application.dataPath, SavePath);
        fileFullPath = Path.Combine(fileFullPath, filename + fileExtension);
        if (File.Exists(fileFullPath)) {
            File.Delete(fileFullPath);
        }
    }

    public static string[] GetDramasName() {
        string fileFullPath = Path.Combine(Application.dataPath, SavePath);
        string[] files = Directory.GetFiles(fileFullPath);
        List<string> dramaName = new List<string>();
        for (int i = 0; i< files.Length; ++i) {
            if (files[i].Contains(fileExtension) == true && files[i].Contains(".meta") == false) {
                dramaName.Add(Path.GetFileName(files[i]).Split('.')[0]);
            }
        }

        /*foreach(string drama in dramaName) {
            Debug.Log(drama);
        }*/

        return dramaName.ToArray();
    }


}
