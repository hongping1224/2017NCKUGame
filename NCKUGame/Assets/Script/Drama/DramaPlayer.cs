﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DramaPlayer : MonoBehaviour {

    #region Singleton

    private static DramaPlayer _self;

    public static DramaPlayer self {
        set {
            _self = value;
        }
        get {
            if (_self == null) {
                GameObject player = (GameObject)Instantiate(Resources.Load("Prefab/Drama/DramaPlayer"));
                _self = player.GetComponent<DramaPlayer>();
            }
            return _self;
        }
    }


    private DramaPlayer() {

    }

    public void Awake() {
        DontDestroyOnLoad(gameObject);
        _self = this;
    }
    #endregion

    #region Canvas
    [SerializeField]
    private Canvas backgroundCanvas;
    [SerializeField]
    private Canvas midgroundCanvas;
    [SerializeField]
    private Canvas foregroundCanvas;

    public Canvas BackgroundCanvas {
        get { return backgroundCanvas; }
    }

    public Canvas MidgroundCanvas {
        get { return midgroundCanvas; }

    }

    public Canvas ForegroundCanvas {
        get { return foregroundCanvas; }
    }
    #endregion


    Queue<BaseDramaEvent> Drama = new Queue<BaseDramaEvent>();
    public void StartDrama(string DramaName) {
        Drama = LoadDrama(DramaName);
        if (Drama != null) {
            StartCoroutine(Drama.Peek().Play());
        }
    }

    public Queue<BaseDramaEvent> LoadDrama(string DramaName) {
        BaseDramaEvent[] eventArray = DramaFileManager.LoadDrama(DramaName);
        if (eventArray != null) {
            Queue<BaseDramaEvent> eventQueue = new Queue<BaseDramaEvent>(eventArray);
            return eventQueue;
        }
        return null;
    }

    private void Update() {

        while (Drama.Count > 0) {
            BaseDramaEvent e = Drama.Peek();

            if (e.WaitTillEnd == true && e.IsRunning == true) {
                break;
            }

            Drama.Dequeue();

            if (Drama.Count > 0) {
                StartCoroutine(Drama.Peek().Play());
            }
        }

    }
}
