﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DramaPreview : MonoBehaviour {

    string DramaName = "";

    private void OnGUI() {
        GUILayout.Label("DramaName ");
        DramaName = GUILayout.TextField(DramaName);
        if (GUILayout.Button("Play")) {
            DramaPlayer.self.StartDrama(DramaName);
        }
            
    }


}
