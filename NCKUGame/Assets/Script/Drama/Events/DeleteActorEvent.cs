﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteActorEvent : BaseDramaEvent {

    public string actorName;
    public float delay;
    public DeleteActorEvent() : this(""){
    }

    public DeleteActorEvent(string actorName ,float delay = 0, bool WaitTillEnd = false) {
       this.actorName = actorName;
       this.WaitTillEnd = WaitTillEnd;
       this.delay = delay;
    }

    public override IEnumerator Play() {
        Transform actor = DramaPlayer.self.MidgroundCanvas.transform.Find(actorName);
        if (actor != null) {
            GameObject.Destroy(actor.gameObject,delay);
            yield return null;
        }
    }

}
