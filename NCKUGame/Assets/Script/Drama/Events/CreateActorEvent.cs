﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;


public class CreateActorEvent : BaseDramaEvent {

    public string texturePath;
    public string actorName;
    public Vector2 position;
    private const string ActorPrefabPath = "Prefab/Drama/Actor";

    public CreateActorEvent() {
        this.texturePath = "";
        this.actorName = "";
        this.position = Vector2.zero;
        EventType = BaseDramaEvent.DramaEventType.CreateActorEvent;
        WaitTillEnd = false;
    }

    public CreateActorEvent(string TexturePath, string ActorName) : this(TexturePath, ActorName, Vector2.zero) {
    }

    public CreateActorEvent(string TexturePath, string ActorName, Vector2 position) {
        this.texturePath = TexturePath;
        this.actorName = ActorName;
        this.position = position;
        WaitTillEnd = false;
        EventType = BaseDramaEvent.DramaEventType.CreateActorEvent;
    }

#if UNITY_EDITOR

    private Texture texture;
    public override void DrawEditor() {
        base.DrawEditor();
        EditorGUILayout.LabelField("ActorName", GUILayout.Width(100));
        actorName = EditorGUILayout.TextField(actorName, GUILayout.Width(100));

        EditorGUILayout.LabelField("TexturePath", GUILayout.Width(120));
        
        if(texture == null) {
            // 讀取路徑圖片用來顯示
            texture = Resources.Load<Texture>(texturePath);
        }
        texture = (Texture)EditorGUILayout.ObjectField(texture, typeof(Texture), false, GUILayout.Width(120));
        //把更新後的圖的路徑 assign 給 texturePath
        //如果有圖片
        if (texture != null) {
            string tempPath = AssetDatabase.GetAssetPath(texture).Substring(17);
            tempPath = tempPath.Remove(tempPath.IndexOf('.'));
            texturePath = tempPath;
        }
        position = EditorGUILayout.Vector2Field("position", position, GUILayout.Width(100));
    }
#endif


    public override IEnumerator Play() {
        GameObject g = (GameObject)GameObject.Instantiate(Resources.Load(ActorPrefabPath), DramaPlayer.self.MidgroundCanvas.transform);
        Sprite sprite = Resources.Load<Sprite>(texturePath);
        g.name = actorName;
        g.transform.localPosition = position;
        g.GetComponent<Image>().sprite = sprite;
        yield return null;
    }

}
