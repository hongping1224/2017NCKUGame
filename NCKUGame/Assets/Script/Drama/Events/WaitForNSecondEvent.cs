﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class WaitForNSecondEvent : BaseDramaEvent {

    public float Second;

    public WaitForNSecondEvent() {
        Second = 0;
        WaitTillEnd = true;
        EventType = BaseDramaEvent.DramaEventType.WaitForNSecondEvent;

    }
    public WaitForNSecondEvent(float Second) {
        this.Second = Second;
        WaitTillEnd = true;
        EventType = BaseDramaEvent.DramaEventType.WaitForNSecondEvent;
    }

#if UNITY_EDITOR
    public override void DrawEditor() {
        base.DrawEditor();
        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.LabelField("Second",GUILayout.Width(100));
            Second = EditorGUILayout.FloatField(Second, GUILayout.Width(100));
        }
        EditorGUILayout.EndHorizontal();
    }
#endif
    public override IEnumerator Play() {
        Debug.Log("Wait for" + Second + " second.");
        IsRunning = true;
        yield return new WaitForSeconds(Second);
        IsRunning = false;
        Debug.Log("Wait Done");
    }


}
