﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ActorMoveEvent : BaseDramaEvent {

    public string actorName;
    public Vector2 MoveTo;
    public float duration;

    public ActorMoveEvent() {
        actorName = "";
        MoveTo = Vector2.zero;
        duration = 0;
    }
    public ActorMoveEvent(string actorName, Vector2 MoveTo, float duration,bool WaitTillEnd = true) {
        this.actorName = actorName;
        this.MoveTo = MoveTo;
        this.duration = duration;
        this.WaitTillEnd = WaitTillEnd;
    }

    public override IEnumerator Play() {
        Transform actor = DramaPlayer.self.MidgroundCanvas.transform.Find(actorName);
        if (actor != null) {
            Tweener tweener = actor.DOLocalMove(MoveTo, duration);
            //如果需要等到結束才進行下一個事件
            if (WaitTillEnd == true) {
                IsRunning = true;
                while (tweener.IsPlaying()) {
                    yield return null;
                }
                IsRunning = false;
            }
        }
    }
}
