﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayBGMEvent : BaseDramaEvent {

    public string BGMName;
    private const string BGMResourcesPath = "Audio/BGM/";
#if UNITY_EDITOR
    public override void DrawEditor() {

    }
#endif
    public override IEnumerator Play() {
        AudioClip bgm = Resources.Load<AudioClip>(BGMResourcesPath + BGMName);
        BGMPlayer.self.ChangeBGM(bgm);
        yield return null;
    }
}
