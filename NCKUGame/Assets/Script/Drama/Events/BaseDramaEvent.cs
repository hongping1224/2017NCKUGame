﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using UnityEditor;
using System;

[XmlInclude(typeof(CreateActorEvent))]
[XmlInclude(typeof(WaitForNSecondEvent))]
[XmlInclude(typeof(WaitForPlayerInputEvent))]
public class BaseDramaEvent {

    protected BaseDramaEvent() {

    }
   
    public bool WaitTillEnd;
    public bool IsRunning;

    public virtual IEnumerator Play() {
        yield return null;
    }

#if UNITY_EDITOR
    public enum DramaEventType {
        CreateActorEvent,
        WaitForNSecondEvent,
        WaitForPlayerInputEvent,
    }
    public DramaEventType EventType;


    //1. override DrawEditor
    //2. base.DrawEditor()
    //3. 檢查有什麼變數。把對應的變數一個個補上編輯功能。
    //4. 美觀
    public virtual void DrawEditor() {
        // EditorGUILayout.LabelField(this.ToString(),GUILayout.Width(150));
       
    }

    public static BaseDramaEvent CreateEvent(DramaEventType type) {
        Type t = Type.GetType(type.ToString());
        return (BaseDramaEvent)Activator.CreateInstance(t);
    }

#endif

}
