﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StupidEvent : BaseDramaEvent {


    public override IEnumerator Play() {
        for (int i = 0; i < 1000000; i++) {
            Debug.Log(i);
            if(i%10 == 0) {
                yield return null;
            }
        }
    }
}
