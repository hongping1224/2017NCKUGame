﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class WaitForPlayerInputEvent : BaseDramaEvent {

    public WaitForPlayerInputEvent() {
        WaitTillEnd = true;
        EventType = BaseDramaEvent.DramaEventType.WaitForPlayerInputEvent;
    }

    public override IEnumerator Play() {
        IsRunning = true;
        Debug.Log("Wait for player input");

        while (true) {
            if (Input.anyKeyDown) {
                Debug.Log("Click");
                break;
            }
            yield return null;
        }

        IsRunning = false;

    }
}
