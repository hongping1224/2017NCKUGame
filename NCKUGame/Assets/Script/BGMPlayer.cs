﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMPlayer : MonoBehaviour {

    #region Singleton

    private static BGMPlayer _self;

    public static BGMPlayer self {
        set {
            _self = value;
        }
        get {
            if (_self == null) {
                GameObject player = (GameObject)Instantiate(Resources.Load("Prefab/BGMPlayer"));
                _self = player.GetComponent<BGMPlayer>();
            }
            return _self;
        }
    }


    private BGMPlayer() {

    }

    public void Awake() {
        DontDestroyOnLoad(gameObject);
        _self = this;
    }
    #endregion


    [SerializeField]
    private AudioSource Source;

    public void ChangeBGM(AudioClip clip) {
        Source.clip = clip;
        Source.Play();
    }

}
