﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnChangeScene : MonoBehaviour {

    private void Awake() {
        DontDestroyOnLoad(gameObject);
    }
}
