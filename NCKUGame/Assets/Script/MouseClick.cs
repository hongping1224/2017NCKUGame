﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClick : MonoBehaviour {

    public GameObject selectedObject;
    public Vector3 OriginalPostion;
    public void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                selectedObject = hit.collider.gameObject;
                OriginalPostion = selectedObject.transform.position;
            }
        }

        if(Input.GetMouseButtonUp(0)) {
            selectedObject.transform.position = OriginalPostion;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
              GameObject OndropObject = hit.collider.gameObject;
               OnItemDrop d =   OndropObject.GetComponent<OnItemDrop>();
                if(d != null) {
                    d.DoSomething();
                }
            }
            selectedObject = null;
        }

        if (selectedObject != null) {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 90;
            selectedObject.transform.position = pos;
        }

    }

}
