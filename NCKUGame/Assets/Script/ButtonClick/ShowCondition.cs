﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCondition : MonoBehaviour {
    public string ConditionName;

    public void Start() {
        if (PlayerDataManager.instance.Progress.ContainsKey(ConditionName)) {
            if (PlayerDataManager.instance.Progress[ConditionName]) {
                gameObject.SetActive(false);
            }
        } 
    }
}
