﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class MoveObject : MonoBehaviour {

    public GameObject target;
    public Vector3 MoveTo;
    
	public void OnClick() {
        //target.transform.DOMove(MoveTo, 2f);
        target.transform.DOLocalMove(MoveTo, 2f);
    }
}
