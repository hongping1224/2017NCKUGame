﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeProgress : MonoBehaviour {

    public void ToggleProgress(string ProgressName) {
        if (PlayerDataManager.instance.Progress.ContainsKey(ProgressName)) {
            PlayerDataManager.instance.Progress[ProgressName] = !PlayerDataManager.instance.Progress[ProgressName];
        }
     //   PlayerData.instance.ClickOldMan = !PlayerData.instance.ClickOldMan;

    }

}
