﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateObject : MonoBehaviour {

    public void CreateGameObject(GameObject prefab) {
        GameObject go = Instantiate(prefab);
        go.transform.parent = transform.parent;
    }
}
