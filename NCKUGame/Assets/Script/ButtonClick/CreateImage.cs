﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateImage : MonoBehaviour {

    public int numA;
    public string stringA;
    public Sprite image;

    public void ButtonClick() {
        Debug.Log(numA);
        Debug.Log(stringA);
        Image img = GetComponent<Image>();
        img.sprite = image;
    }

}
